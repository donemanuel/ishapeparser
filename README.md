# Welcome to iShapeParser

With iShapeParser you can vectorize Keynote shapes.

## What for?

If you want to export one of the 750 provided shapes from Keynote into a svg file to use elsewhere (ex. Web design) this is the right tool for you.

## Licencing?
Please keep in mind that all of the SVG’s that you get through this method maybe have a copyright! It is unknown to me under what copyright they fall under as they are clearly  made by apple but also for the public domain, so if you may use them in your own public projects? Maybe contact Apple for that, but for now I will just suppose that it is allowed! (Since when you use Keynote (or any other iWork app) you design presentations which are meant to be seen by the world. and you can edit those shapes to form “My Shapes” so I don’t really see any limits on how you may use this.)


## HowTo

To learn how to use the iShapeParser script, please check the HOWTO pdf provided.
If you want me to do it wihin the README.md please open an issue.

